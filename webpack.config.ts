import * as webpack from 'webpack';
import * as path from 'path';

const isProduction = process.env.NODE_ENV === 'production';

const config: webpack.Configuration = {
    entry: './src/index.ts',
    output: {
        filename: 'game.js',
        path: path.resolve(__dirname, 'dist'),
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            marked: 'marked',
        }),
        new webpack.NamedModulesPlugin(),
    ],

    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: ['.ts', '.js']
    },
    module: {
        loaders: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
            },

            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader", options: {
                            sourceMap: !isProduction,
                        },
                    },
                    {
                        loader: "sass-loader", options: {
                            sourceMap: !isProduction,
                        },
                    },
                ],
            },
        ],
    },

    devtool: '#@eval-source-map',
};

if (isProduction) {
    config.plugins.push(
        new webpack.HotModuleReplacementPlugin(),
    )
}

export default config;
