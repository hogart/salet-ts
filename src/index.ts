import 'salet';
import * as $ from 'jquery';

import { rooms, units } from './utils/declare';
import { bus } from './utils/bus';
import { Inventory } from './utils/Inventory';
import './utils/cls';

window.salet.game_id = 'your-game-id-here';
window.salet.game_version = '0.0.1';
window.salet.autosave = false;
window.salet.start = 'north';

const inventory = new Inventory();
bus.on('afterAction', () => {
    inventory.display();
});

const gameUnits = [
    {
        name: 'whiteBear',
        location: 'south',
        dsc: 'Чучело {{белого медведя}}',
        takeable: true,
        display: 'Чучело',
    },
];

const gameRooms = [
    {
        name: 'north',
        dsc: `
## Северная комната

Просторная белая комната.
`,
        title: 'Северная комната.',
        ways: ['east', 'west'],
    },
    {
        name: 'south',
        title: 'Южная комната',
        dsc: `
  ## Южная комната

  Комната со стеклянным потолком.
`,
        ways: ['east', 'west'],
    },
    {
        name: 'east',
        title: 'Восточная комната',
        dsc: '## Восточная комната',
        ways: ['south', 'north'],
    },
    {
        name: 'west',
        title: 'Западная комната',
        dsc: '## Западная комната',
        ways: ['south', 'north']
    }
];

rooms(gameRooms);
units(gameUnits);

$(() => {
    window.salet.beginGame();
});
