import 'salet';
import { bus } from './bus';

// this module clears room description on every action
// so taken objects are not displayed there anymore

bus.on('beforeAction', () => {
    window.salet.view.clearContent();
});

bus.on('afterAction', () => {
    const r = window.salet.here();
    r.entering(r.name);
});
