import 'salet';
import * as $ from 'jquery';

export const bus = $(document);

window.salet.beforeAction = function (name: TRoomName, action: TActionName) {
    bus.trigger('beforeAction',
        {
            name,
            action,
        }
    );
};

window.salet.afterAction = function(name: TRoomName, action: TActionName) {
    bus.trigger('afterAction',
        {
            name,
            action,
        }
    );
};
