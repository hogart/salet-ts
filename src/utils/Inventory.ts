import 'salet';

export class Inventory {
    el: Element;

    constructor(element = '#inventory') {
        this.setElement(element);
    }

    protected setElement(element = '#inventory'): void {
        this.el = document.querySelector(element)
    }

    protected renderItem(accumulator: string, thing: SaletUnit, i: number): string {
        return `
            ${accumulator}
            <li>${window.salet.character.listinv(thing.name)}<li>
        `;
    }

    protected render() {
        const content = window.salet.character.inventory.reduce(
            this.renderItem,
            ''
        );

        return `
            <ul class="salet-inventory">
                ${content}
            </ul>`;
    }

    public display() {
        this.el.innerHTML = this.render();
    }
}
