import 'salet';

export function rooms(rooms: SaletRoomProps[]) {
    rooms.forEach(roomDescr => {
        window.room(roomDescr.name, roomDescr);
    });
}

export function units(units: SaletUnitProps[]) {
    units.forEach(unitDescr => {
        const unitInstance = window.unit(unitDescr.name, unitDescr);
        if (unitDescr.location) {
            unitInstance.put(unitDescr.location);
        }
    })
};
